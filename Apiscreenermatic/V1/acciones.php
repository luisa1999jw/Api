<?php
ob_start();
require_once 'class/respuestas.class.php';
require_once 'class/acciones.class.php';

$_acciones= new acciones;
$_respuestas =new respuestas;

$_respuestas->headerHttpPro($_SERVER['HTTP_ORIGIN']);

if($_SERVER['REQUEST_METHOD'] == "GET"){
    //recibimos
    $headers = getallheaders();
    if(isset($headers["Token"])){
        //recibimos los datos enviados por el header
        $send = [
            "Token" => $headers["Token"]
        ];
        $postBody = json_encode($send);
    }else{
        //recibimos los datos enviados
        $postBody = file_get_contents("php://input");
    }
    //enviamos
    $res = $_acciones-> mostrarName($postBody);
   
     
}else{
   header('Content-Type: application/json');
   $datosArray = $_respuestas->error_405();
   echo json_encode($datosArray);
}

?>
