<?php
ob_start();
require_once 'conexion/conexion.php';
require_once 'respuestas.class.php';

class acciones extends conexion{

    private $table ="carga_acciones";
    private $token = "";
    //private $estado= "";

    public function mostrarName($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            header('Content-Type: application/json');
            $datosA = $_respuestas->error_401("Error en el item 'Token'");
            echo json_encode($datosA);
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                    if(isset($_GET["ticker"])){
                    $pagina=  $_GET["ticker"];
                    $listaPacientes = $this-> listaAcciones($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                    'Ticker'=> $value['ac_ticker'],
                    'Current ratio' => $value ['ac_current'],
                    'Quick Ratio' => $value ['ac_quick'],
                    'Cash Ratio' => $value ['ac_cash'],
                    'Book Value per Share' => $value ['ac_book'],
                    'Book Value Tangible per Share' => $value ['ac_debt'],
                    'Debt to equity' => $value ['ac_long'],
                    'Asset to equity' => $value ['ac_asset'],
                    'Interesr Coverage' => $value ['ac_return'],
                    'Return on  Assets(%)' => $value ['ac_returneq'],
                    'Return on Equity(%)' => $value ['ac_roic'],
                    'Return on Invested Capital' => $value ['ac_roce'],
                    'Total asset turnover' => $value ['ac_total'],
                    'Working capital turn over' => $value ['ac_working'],
                    'Inventory turn over,' => $value ['ac_inventory'],
                    'Receivables turn over' => $value ['ac_recievables'],
                    'Payables turnover' => $value ['ac_payables'],
                    'Days inventories outstanding' => $value ['ac_daysinventories'],
                    'Days sales outstanding' => $value ['ac_dayssales'],
                    'Days payable outstanding' => $value ['ac_dayspayables'],
                    'DOL' => $value ['ac_dol'],
                    'DFL' => $value ['ac_dfl'],
                    'Cash conversion cycle' => $value ['ac_cachconversion'],
                    'Gross Mg TTM' => $value ['ac_grossmg'],
                    'Operating Mg TTM' => $value ['ac_operatingmg'],
                    'Earnings oer Share' => $value ['ac_earning'],
                    'Book value TTM' => $value ['ac_bookvaluettm'],
                    'Book value TTM' => $value ['ac_bookvaluettm'],
                    'Book value tangible TTM' => $value ['ac_bookvaluetangible'],
                    'Sales per Share' => $value ['ac_salesttm'],
                    'Clashflow per Share' => $value ['ac_fcfettm'],
                    'Last quarter reported' => $value ['ac_quart'],
                    'Shares outstanding' => $value ['ac_sharesoutstanding'],
                    'Gross income TTM' => $value ['ac_grossinc'],
                    'Operating income TTM' => $value ['ac_opincome'],
                    'Net income TTM' => $value ['ac_netinc'],
                    'Minority Interest TTM' => $value ['ac_minority'],
                    'Net income shareholders TTM' => $value ['ac_netincsh'],
                    'Comprenhensive income shareholders TTM' => $value ['ac_compinc'],
                    'Revenue TTM' => $value ['ac_revenue'],
                    'Cashflow Operating TTM'=>  $value ['ac_cfop'],
                    'Cashflow Investing TTM' => $value ['ac_cfinv'],
                    'Cashflow Financing TTM' => $value ['ac_cffin'],
                    'Cashend of period TTM' => $value ['ac_cashend'],
                    'Current assets TTM' => $value ['ac_currass'],
                    'Non current assets TTM' => $value ['ac_noncurrass'],
                    'Total assets TTM' => $value ['ac_totass'],
                    'Current liabilities TTM' => $value ['ac_currliab'],
                    'Total liabilities TTM' => $value ['ac_totliab'],
                    'Non current liabilities TTM' => $value ['ac_noncurrliab'],
                    'Past Earnings date' =>$value['ac_ultimoearnin'],

                        );
                         header("Content-Type: application/json");
                         echo json_encode($array);
                         http_response_code(200);
                    }
                }
                }else{
                header('Content-Type: application/json');
                $datosA = $_respuestas->error_401("No tiene  acceso a este servicio");
                echo json_encode($datosA);
                }
                }else{
                header('Content-Type: application/json');
                $datosA = $_respuestas->error_401("El token que envio es invalido o ha caducado");
                echo json_encode($datosA);
            }
        }
    }

    private function listaAcciones($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  ac_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }

    
    private function buscarToken(){
        $query = "SELECT * FROM  clientes WHERE cli_tokenApi = '" . $this->token . "' ";
        $resp = parent::obtenerDatos($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }
    private function obtenerDatosusuario($id){
        $query = "SELECT * FROM clientes WHERE cli_id = '$id' AND api = '1' ";
        $datos = parent::obtenerDatos($query);
        if($datos){
            return $datos;
        }else{
            return 0;
        }
    
    
    }
    
    
}
?>