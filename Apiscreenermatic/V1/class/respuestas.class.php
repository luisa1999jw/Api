<?php
ob_start();
require_once "conexion/conexion.php";

class respuestas extends conexion{

    
    public  $response = [
        'status' => "ok",
        "result" => array()
    ];
    public function headerHttpPro($origin){
        if(!isset($origin)){
            die(json_encode("No hemos encontrado un origen valido"));
        }
        $origin1=$this->buscarDominio($origin);
        $data = json_encode($origin1);
        $array = json_decode($data,true);    
        foreach ($array as $key => $value){
            $array = array(       
         $value['cli_dominio']
    
            );
    }
     
    //$list=['http://www.apirestscreenmatic.com'];
    
    if(in_array($origin,$array)){

    echo header("Access-Control-Allow-Origin: $origin");
    echo header("Access-Control-Allow-Methods:GET,POST");
    echo header("Allow: GET, POST");
    echo header("Acess-Control-Allow-Headers:X-API-KEY,Origin, X-Requested-With, Content-Type,Acept");
    echo header('Content-Type: application/json');

    }else{
        die(json_encode("Su dominio no tiene  permitido acceso a la API"));
    }
}
    
    final public static function headerHttpDev($origin){
        if ($method == 'OPTIONS'){
         exit(0);
    }

    echo header("Access-Control-Allow-Origin: *");
    echo header("Access-Control-Allow-Methods:GET,POST");
    echo header("Allow: GET, POST");
    echo header("Acess-Control-Allow-Headers:X-API-KEY,Origin, X-Requested-With, Content-Type,Acept");
    echo header('Content-Type: application/json');
    }
    public function error_405(){
        $this->response['status'] = "error";
        $this->response['result'] = array(
            "error_id" => "405",
            "error_msg" => "Metodo no permitido"
        );
        return $this->response;
    }

    public function error_200($valor = "Datos incorrectos"){
        $this->response['status'] = "error";
        $this->response['result'] = array(
            "error_id" => "200",
            "error_msg" => $valor
        );
        return $this->response;
    }


    public function error_400(){
        $this->response['status'] = "error";
        $this->response['result'] = array(
            "error_id" => "400",
            "error_msg" => "Datos enviados incompletos o con formato incorrecto"
        );
        return $this->response;
    }


    public function error_500($valor = "Error interno del servidor"){
        $this->response['status'] = "error";
        $this->response['result'] = array(
            "error_id" => "500",
            "error_msg" => $valor
        );
        return $this->response;
    }


    public function error_401($valor = "No autorizado"){
        $this->response['status'] = "error";
        $this->response['result'] = array(
            "error_id" => "401",
            "error_msg" => $valor
        );
        return $this->response;
    }
    
    private function select(){
    $query="SELECT DominioFROM clientes_token";
    $datos = parent::obtenerDatos($query);
    return ($datos);
    }
    private function buscarDominio($origin){
        $query = "SELECT * from clientes WHERE cli_dominio =   '$origin' " ;
        $resp = parent::obtenerDatos($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }

   
     
    
    
    

}
?>