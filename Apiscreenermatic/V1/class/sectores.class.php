<?php
ob_start();
require_once "conexion/conexion.php";
require_once "respuestas.class.php";

class sectores extends conexion{

    private $table ="sectoresarg";
    private $token = "";

    public function verSector($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            header('Content-Type: application/json');
            $datosA = $_respuestas->error_401("Error en el item 'Token'");
            echo json_encode($datosA);
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["sector"])){
                    $pagina=  $_GET["sector"];
                    $listaSector = $this-> listaSector($pagina);
                    $data = json_encode($listaSector);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                    'Sector'=> $value['sector'],
                    'Sectorsp'=> $value['sectoresp'],
                    'Current ratio' => $value ['currentr'],
                    'Earnings per Share' => $value ['eps'],
                    'Return on Assets (%)' => $value ['roa'],
                    'Return on Equity(%)' => $value ['roe'],
                    'Shares OutStading' => $value ['shrsout'],
                    'Renueve  TTM' => $value ['revenue'],
                    'Gross Income TTM' => $value ['grossinc'],
                    'Operating Income TTM' => $value ['opincome'],
                    'Net Income Shareholders TTM' => $value ['netinc'],
                    'Cashflow Operating TTM' => $value ['cfo'],
                    'Cashflow Investing TTM' => $value ['cfi'],
                    'Cashflow Financing TTM' => $value ['cff'],
                    'Cashend of period TTM' => $value ['cashend'],
                    'Current Asset TTM' => $value ['currass'],
                    'Non Current Liabilities TTM' => $value ['noncurrass'],
                    'Total Assets TTM' => $value ['assets'],
                    'Current Liabilities TTM' => $value ['currliab'],
                    'Non current Liabilities TTM' => $value ['noncurrliab'],
                    'Total Liabilities TTM' => $value ['liabilities'],
                    'Shareholders Equity TTM' => $value ['bookval'],
                    //'Book Value per Share' => $value ['bvps'],
                    //'Book Value Tangible per Share' => $value ['bvtps'],
                    //'Sales per share' => $value ['sps'],
                    //'Cashflow per share' => $value ['cfps'],
                        );
                         header("Content-Type: application/json");
                         echo json_encode($array);
                         http_response_code(200);
                    }

                }
            }else{
            header('Content-Type: application/json');
            $datosA = $_respuestas->error_401("No tiene  acceso a este servicio");
            echo json_encode($datosA);
            }
            }else{
            header('Content-Type: application/json');
            $datosA = $_respuestas->error_401("El token que envio es invalido o ha caducado");
            echo json_encode($datosA);
        }
    }
}
    private function listaSector($sector){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  sectoresp = '$sector' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    private function buscarToken(){
        $query = "SELECT * FROM  clientes WHERE cli_tokenApi = '" . $this->token . "' ";
        $resp = parent::obtenerDatos($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }
    private function obtenerDatosusuario($id){
        $query = "SELECT * FROM clientes WHERE cli_id = '$id' AND api2 = '1' ";
        $datos = parent::obtenerDatos($query);
        if($datos){
            return $datos;
        }else{
            return 0;
        }
    
    
    }
}
?>