<?php
ob_start();
require_once "conexion/conexion.php";
require_once "respuestas.class.php";

class excel extends conexion{

    private $table ="articulos";
    private $token = "";

    public function dowCashflowAnualSp($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["xlsx/es/anual"])){
                    $pagina= $_GET["xlsx/es/anual"];
                    $listaPacientes = $this-> xlxsAnualEsp($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_archivo2esp'],
                
                        );
                        
                        
                    }
                         header("Content-Type: application/json");
                         $res= implode($array);
                         $file="https://screenermatic.com/screener/filedata/$res";
                         // define file $mime type here
                         $mime="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                         ob_end_clean(); // this is solution
                         //ob_start();
                         header('Content-Description: File Transfer');
                         header('Content-Type: ' . $mime);
                         header("Content-Transfer-Encoding: Binary");
                         header("Content-disposition: attachment; filename=\"" . basename($file) . "\"");
                         header("Content-Transfer-Encoding: binary");
                         header("Expires: 0");
                         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                         header('Pragma: public');
                         
                         ob_clean();
                         flush();
                         
                         $rest=$this->url_get_contents($file);
                         echo $rest;
                         exit;

                }
            }else{
            
            }
            }else{
            header('Content-Type: application/json');
            $datosA = $_respuestas->error_401("El token que envio es invalido o ha caducado");
            echo json_encode($datosA);
        }
    }
}
    private function xlxsAnualEsp($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    public function dowCashflowQuaSp($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            header('Content-Type: application/json');
            $datosA = $_respuestas->error_401("Error en el item 'Token'");
            echo json_encode($datosA);
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["xlsx/es/quater"])){
                    $pagina= $_GET["xlsx/es/quater"];
                    $listaPacientes = $this-> xlxsQuaEsp($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_archivo2triesp'],
                
                        );
                    }
                         header("Content-Type: application/json");
                         $res= implode($array);
                         $file="https://screenermatic.com/screener/filedata/$res";
                         // define file $mime type here
                         $mime="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                         ob_end_clean(); // this is solution
                         //ob_start();
                         header('Content-Description: File Transfer');
                         header('Content-Type: ' . $mime);
                         header("Content-Transfer-Encoding: Binary");
                         header("Content-disposition: attachment; filename=\"" . basename($file) . "\"");
                         header("Content-Transfer-Encoding: binary");
                         header("Expires: 0");
                         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                         header('Pragma: public');
                         
                         ob_clean();
                         flush();
                         
                         $rest=$this->url_get_contents($file);
                         echo $rest;
                         //exit();   
                    

                   
                }
            }else{
            
            }
            }else{
            
        }
    }
}
    private function xlxsQuaEsp($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    ////rutas en ingles
    public function dowCashflowAnual($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
           
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["xlsx/en/anual"])){
                    $pagina= $_GET["xlsx/en/anual"];
                    $listaPacientes = $this-> xlxsAnual($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_archivo2'],
                
                        );
                    }
                         header("Content-Type: application/json");
                         $res= implode($array);
                         $file="https://screenermatic.com/screener/filedata/$res";
                         // define file $mime type here
                         $mime="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                         ob_end_clean(); // this is solution
                         //ob_start();
                         header('Content-Description: File Transfer');
                         header('Content-Type: ' . $mime);
                         header("Content-Transfer-Encoding: Binary");
                         header("Content-disposition: attachment; filename=\"" . basename($file) . "\"");
                         header("Content-Transfer-Encoding: binary");
                         header("Expires: 0");
                         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                         header('Pragma: public');
                         
                         ob_clean();
                         flush();
                         
                         $rest=$this->url_get_contents($file);
                         echo $rest;
                         //exit();  
                    

                   
                }
            }else{
            
            }
            }else{
            
        }
    }
}
    private function xlxsAnual($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    public function dowCashflowQua($json){
        $_respuestas = new respuestas;
        $datos =json_decode($json, true);

        if(!isset($datos['Token'])){
            
        }else{
            $this->token=$datos['Token'];
            //$this->cli_id=$datos['cli_Id'];
            $arrayToken=$this->buscarToken();
            if($arrayToken){
                //$usuario= $datosStatus ['usuario'];
                $datosStatus=$this->obtenerDatosusuario($arrayToken[0]['cli_id']);
                if($datosStatus){
                if(isset($_GET["xlsx/en/quater"])){
                    $pagina= $_GET["xlsx/en/quater"];
                    $listaPacientes = $this-> xlxsQua($pagina);
                    $data = json_encode($listaPacientes);
                    $array = json_decode($data,true);
                    foreach ($array as $key => $value){
                        $array = array(       
                     $value['art_archivo2tri'],
                
                        );
                    }
                         header("Content-Type: application/json");
                         $res= implode($array);
                         $file="https://screenermatic.com/screener/filedata/$res";
                         ob_end_clean();
                         // define file $mime type here
                         $mime="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                         header('Content-Description: File Transfer');
                         header('Content-Type: ' . $mime);
                         header("Content-Transfer-Encoding: Binary");
                         header("Content-disposition: attachment; filename=\"" . basename($file) . "\"");
                         header("Content-Transfer-Encoding: binary");
                         header("Expires: 0");
                         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                         header('Pragma: public');
                         
                         ob_clean();
                         flush();
                         
                         $rest=$this->url_get_contents($file);
                         echo $rest;
                                               
                }
            }else{
            
            }
            }else{
            
        }
    }
}
    private function xlxsQua($names){
    
        $query = "SELECT * FROM " . $this->table . " WHERE  art_ticker = '$names' ";
        $datos = parent::obtenerDatos($query);
        return ($datos);
    }
    private function buscarToken(){
        $query = "SELECT * FROM  clientes WHERE cli_tokenApi = '" . $this->token . "' ";
        $resp = parent::obtenerDatos($query);
        if($resp){
            return $resp;
        }else{
            return 0;
        }
    }
    private function url_get_contents ($Url) {
    if (!function_exists('curl_init')){ 
        die('CURL is not installed!');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
} 
private function obtenerDatosusuario($id){
    $query = "SELECT * FROM clientes WHERE cli_id = '$id' AND fstatdownload = '1' ";
    $datos = parent::obtenerDatos($query);
    if($datos){
        return $datos;
    }else{
        return 0;
    }


}

}
ob_end_flush();
?>