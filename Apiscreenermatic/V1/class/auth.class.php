<?php
ob_start();
require_once 'conexion/conexion.php';
require_once 'respuestas.class.php';

class auth extends conexion{

    public function login($json){
      
        $_respustas = new respuestas;
        $datos = json_decode($json,true);

        if(!isset($datos['Usuario']) || !isset($datos["Password"])){
            //error con los campos
            return $_respustas->error_400();
        }else{
            //todo esta  bien
            $usuario= $datos ['Usuario'];
            $password= $datos ['Password'];
            $datos = $this->obtenerDatosusuario($usuario);
            if($datos){
                if(password_verify($password,$datos [0]['cli_pass'])){
                    if($datos[0]['api']== "1" || $datos [0]['api2']=="1" || $datos [0]['api3']=="1" || $datos [0]['fstat']=="1" || $datos [0]['fstatdownload']=="1"){
                       
                        $verificar = $this-> insertarToken($datos[0]['cli_id']);
                        if($verificar){
                            //si se guardo
                            $result = $_respustas->response;
                            $result ["result"]= array(
                            "Token"=>$verificar);
                            return $result;
                        }else{
                            //si no se guardo
                            return $_respustas->error_500("Error interno, no hemos podido guardar");
                        }
                       
                    }else{
                         //si esta inactivo
                    
                    return $_respustas->error_200("El usuario no Activo ningun servicio");

                    }
            }else{
                return $_respustas->error_200("La contraseña es incorrecta");
            }
        }else{
            //si no existe
            return $_respustas->error_200("El usuario no existe");
        }
    }
    }
   

    private function obtenerDatosusuario($correo){
        $query = "SELECT cli_id,cli_pass,api,api2,api3,fstat,fstatdownload,cli_status FROM clientes WHERE cli_mail = '$correo'";
        $datos = parent::obtenerDatos($query);
        if(isset($datos[0]["cli_id"])){
            return $datos;
        }else{
            return 0;
        }

    }
   

    private function insertarToken($usuarioid){
        $val = true;
        $token = bin2hex(openssl_random_pseudo_bytes(16,$val));
        $date = date("Y-m-d H:i");
        $estado = "Activo";
        $query = "INSERT INTO clientes_token (cli_id, Token, Estado, Fecha)VALUES ('$usuarioid','$token','$estado','$date')";
        $verifica = parent::nonQuery($query);
        if($verifica){
            return $token;
        }else{
            return 0;
        }
    }
    
}



?>