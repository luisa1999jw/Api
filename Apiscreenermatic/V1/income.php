<?php
ob_start();
require_once "class/respuestas.class.php";
require_once "class/imagenes.income.class.php";
require_once "class/excel.income.class.php";

$_respuestas =new respuestas;
$_imagenes= new imagenes;
$_excel=new excel;

$_respuestas->headerHttpPro($_SERVER['HTTP_ORIGIN']);

if($_SERVER['REQUEST_METHOD']=="GET"){
    //recibimos
    $headers = getallheaders();
    if(isset($headers["Token"])){
        //recibimos los datos enviados por el header
        $send = [
            "Token" => $headers["Token"]
        ];
        $postBody = json_encode($send);
    }else{
        //recibimos los datos enviados
        $postBody = file_get_contents("php://input");
    }
    //enviamos
    // imagenes y excel en español
    $res = $_imagenes-> seeIncomeAnualSp($postBody);
    
    $res = $_excel-> dowIncomeAnualSp($postBody);
    
    $res = $_imagenes->seeIncomeQuaSp($postBody);
    
    $res = $_excel->dowIncomeQuaSp($postBody);
    //imagenes y excel en ingles
    $res = $_imagenes-> seeIncomeAnual($postBody);
    
    $res = $_excel-> dowIncomeAnual($postBody);
    
    $res = $_imagenes->seeIncomeQua($postBody);
    
    $res = $_excel->dowIncomeQua($postBody);
   

}else{
   header('Content-Type: application/json');
   $datosArray = $_respuestas->error_405();
   echo json_encode($datosArray);
}

?>
